angular.
    module('page2').
    component('page2', {
        template: [
            '<div ng-init="init()">',
                '<div id="questions">',
                '</div>',
                '<a id="backward" href="/competences/">Редактировать</a>',
                '<b class="loading"></b>',
            '</div>',
            '<style>',
                '.loading {',
                    'position: absolute;',
                    'display: block;',
                    'background: url("http://katushka.in.ua/templates/katushka2/images/ajax-loader.gif") center center no-repeat;',
                    'opacity: 0.7;',
                    'width: 100%;',
                    'height: 100%; }',
            '</style>',  
            ].join(""),
        controller: ['$http', '$routeParams', '$scope',
            function page2Controller($http, $routeParams, $scope) {
                var ctrl = this;

                // Инициализируем локальные переменные контроллера значениями из памяти
                $scope.init = function() {
                    $scope.first_name = localStorage.getItem("comp_first_name");
                    if ($scope.first_name ===  null)
                        $scope.first_name = "";
                    $scope.last_name = localStorage.getItem("comp_last_name");
                    if ($scope.last_name ===  null)
                        $scope.last_name = "";
                    $scope.middle_name = localStorage.getItem("comp_middle_name");
                    if ($scope.middle_name ===  null)
                        $scope.middle_name = "";
                    $scope.nickname = localStorage.getItem("comp_nickname");
                    if ($scope.nickname ===  null)
                        $scope.nickname = "";
                    $scope.email = localStorage.getItem("comp_email");                
                    if ($scope.email ===  null)
                        $scope.email = "";                
                    $scope.about = localStorage.getItem("comp_about");
                    if ($scope.about ===  null)
                        $scope.about = "";                
                    $scope.tags = JSON.parse(localStorage.getItem("comp_tags"));
                    if ($scope.tags ===  null)
                        $scope.tags = [];
                    $scope.phone = localStorage.getItem("comp_phone");
                    if ($scope.phone ===  null)
                        $scope.phone = "";
                    $scope.experience = JSON.parse(localStorage.getItem("comp_experience"));
                    if ($scope.experience ===  null)
                        $scope.experience = [];
                    $scope.birth = localStorage.getItem("comp_birth");
                    if ($scope.birth ===  null)
                        $scope.birth = "";
                    $scope.spec_arr = JSON.parse(localStorage.getItem("comp_spec"));
                    if ($scope.spec_arr ===  null)
                        $scope.spec_arr = [];

                    $scope.profession = JSON.parse(localStorage.getItem("comp_profession"));
                    if ($scope.profession ===  null)
                        $scope.profession = [];

                    $scope.spec_nums = JSON.parse(localStorage.getItem("comp_spec_nums"));
                    if ($scope.spec_nums ===  null)
                        $scope.spec_nums = [];
                    $scope.answers = [];

                    console.log($scope.profession, 'profession')
                                        
                    $scope.csrftoken = Cookies.get('csrftoken')
                
                    // Формируем структуру данных для отправки на сервер
                    var data = {
                        first_name: $scope.first_name,
                        last_name: $scope.last_name,
                        middle_name: $scope.middle_name,
                        username: $scope.nickname,
                        email: $scope.email,
                        about: $scope.about,
                        phone: $scope.phone,
                        date_of_birth: $scope.birth,
                        experience: $scope.experience,
                        tags: $scope.tags,
                        spec: $scope.spec_arr,
                        profession: $scope.profession,
                        spec_nums: $scope.spec_nums
                    }

                    // This is the client-side script.

                    // Initialize the HTTP request.
                    var xhr = new XMLHttpRequest();
                    xhr.open('post', '/create_user/');

                    xhr.setRequestHeader("Content-type", "application/json");
                    xhr.setRequestHeader("X-CSRFToken", $scope.csrftoken);

                    // Track the state changes of the request.
                    xhr.onreadystatechange = function () {                
                        var DONE = 4; // readyState 4 means the request is done.
                        var OK = 200; // status 200 is a successful return.
                       
                        if (xhr.readyState === DONE) {
                            if (xhr.status === OK) {
                                var response = JSON.parse(xhr.responseText);
                                var questions = JSON.parse(response["questions"]); // 'This is the returned text.'
                                var answers = JSON.parse(response["answers"]); // 'This is the returned text.'                          

                                for (var i = 0; i < questions.length; i++) {
                                    $("div#questions").append([
                                        '<hr><div>',
                                            questions[i],
                                            '<textarea id="answer', i, '" class="answer" style="width: 50%; height: 10em; resize: none;" class="questions">',
                                                answers[i],                                                
                                            '</textarea><hr>',
                                            '<div id="error', i,'" class="questions_error" style="visibility: hidden; color: red;">Ответьте на вопрос</div>',
                                        '</div>'].join("")
                                    );
                                }

                                $(".loading").hide();
                                 
                                if (questions.length !== 0) {
                                    $("div#questions").after('<button id="reply">Ответить</button>');
                                    var xhr1;
                                    
                                    $scope.update_questions = function() {
                                        // Initialize the HTTP request.
                                        xhr1 = new XMLHttpRequest();

                                        xhr1.open('post', '/write_answers/');

                                        var csrftoken = Cookies.get('csrftoken');
                                        xhr1.setRequestHeader("Content-type", "application/json");
                                        xhr1.setRequestHeader("X-CSRFToken", $scope.csrftoken);

                                        // Track the state changes of the request.
                                        xhr1.onreadystatechange = function () {

                                            var DONE = 4; // readyState 4 means the request is done.
                                            var OK = 200; // status 200 is a successful return.

                                            if (xhr1.readyState === DONE) {
                                                if (xhr1.status === OK) {
                                                    $("div#questions").append(
                                                        '<hr><div class="questions_log" style="color: green;">Сохранено</div>'
                                                    );                                          
                                                    $scope.$digest();
                                                } else {
                                                    console.log('Error: ' + xhr1.status); // An error occurred during the request.
                                                }
                                            }
                                            $scope.$digest();
                                        }
                                    }
                                                                    
                                    $("button#reply").click(function() {
                                        var answers = document.getElementsByClassName('answer');
                                        var data1 = {}
                                        var SendFlag = true;
                                        
                                        for (var i = 0; i < answers.length; i++) {
                                            if (answers[i].value.replace(/\s/g, '') === "") {
                                                $("div#error" + i).css({"visibility": "visible"});
                                                SendFlag = false;
                                            }
                                            data1[questions[i]] = answers[i].value;
                                        }

                                        if (SendFlag) {
                                            $scope.update_questions();
                                        
                                            // Send the request to ajax data
                                            xhr1.send(JSON.stringify(data1));
                                        }
                                    });
                                } else {
                                    $("div#questions").after('<div>Нет вопросов</div>');
                                }
                                $scope.$digest();
                            } else {
                                console.log('Error: ' + xhr.status); // An error occurred during the request.
                            }
                        }
                    };

                    // Send the request to ajax data
                    xhr.send(JSON.stringify(data));
                }
            }
        ]
    });
