angular.
  module('myApp').
  config(['$locationProvider', '$routeProvider',
    function config($locationProvider, $routeProvider) {
      $locationProvider.hashPrefix('!');

      $routeProvider.
        when('/page1', {
          template: '<page1></page1>'
        }).
        when('/page2', {
          template: '<page2></page2>'
        }).
        otherwise('/page1');
    }
  ]);
