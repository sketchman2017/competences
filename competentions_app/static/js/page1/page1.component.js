angular.
    module('page1').
    component('page1', { // Имя
    template: [
        '<div ng-init="init()">',
        
        '<div class="container-fluid">',
        
            '<div class="row"><div class="col-md-4"><input class="form-control" placeholder="Ваше имя *" id="first_name" ng-model="first_name"></input>',
            '<div id="first_name_error" style="visibility:hidden; color: red;">Укажите имя</div></div>',

            // Фамилия
            '<div class="col-md-4"><input class="form-control" placeholder="Фамилия *" id="last_name" ng-model="last_name"></input>',
            '<div id="last_name_error" style="visibility: hidden; color: red;">Укажите фамилию</div></div>',

            // Отчество
            '<div class="col-md-4"><input class="form-control" placeholder="Отчество" id="middle_name" ng-model="middle_name"></input>',
            '<div id="middle_name" style="visibility: hidden; color: red;">Укажите отчество</div></div></div>',

            // Ник
            '<div class="row"><div class="col-md-4"><input class="form-control" placeholder="Ник" id="nickname" ng-model="nickname"></input>',
            '<div id="middle_name" style="visibility: hidden; color: red;">Укажите ник</div></div>',

            // Email
            '<div class="col-md-4"><input class="form-control" placeholder="Email *" type="email" id="email" ng-model="email"></input>',
            '<div id="email_error" style="visibility: hidden; color: red;">Укажите email</div></div>',

            // Телефон
            '<div class="col-md-4"><div><input class="form-control" placeholder="Номер телефона" type=\'tel\' id="phone" ng-model="phone"></input>',
            '<div id="phone_error" style="visibility: hidden; color: red;">Телефон должен содержать от 5 до 15 цифр и возможен знак плюс</div></div></div>',

        '</div><hr>',

        // Специальность 
        '<div id="spec">',
            '<ul>',
                '<li ng-repeat="prop in props">',                       
                    '<div class="specialization">',
                        '<label for="specialization">Специальность *&nbsp</label>',

                        '<select class="form-control" ng-options="item as item.spec for item in specializations" ng-model="selectedSpecialization[prop]" ng-change="$ctrl.service_item_id[prop] = selectedSpecialization[prop].id;"></select>',

                            // Навыки

                            '<div ng-if="selectedSpecialization[prop]">',
                                '<label ng-if="selectedSpecialization[prop]" for="competentions">Профессиональные навыки *</label>',
                                    '<auto-complete source="loadTags($query)"></auto-complete>',
                                
                                '<div id="competentions_error" style="visibility: hidden; color: red;">Укажите ключевые слова специальности</div>',
                                '<tags-input id="competentions" ng-model="tags[prop]">',
                                '</tags-input>',
                            '</div>',


                            // Опыт работы
                            '<div ng-if="selectedSpecialization[prop]">',
                                '<label for="experience">Опыт работы *&nbsp</label>',
                                '<select class="form-control" id="experience" ng-model="experience[prop]">',
                                    '<option ng-repeat="(key, value) in $ctrl.experience_items track by $index">{{value}}</option>',
                                '</select>',                                
                            '</div>',

                        // Услуги

                            '<label ng-if="selectedSpecialization[prop]">Оказываемые услуги *</label>',                            
                            '<div id="services" ng-if="selectedSpecialization[prop]">',
                                '<label ng-repeat="(key, value) in $ctrl.service_items[prop][$ctrl.service_item_id[prop]] track by $index">',
                                '<input type="checkbox" ng-model="$ctrl.service_items[prop][$ctrl.service_item_id[prop]][key]">{{key}}&nbsp&nbsp</label>',
                            '</div>',


                        '<div id="services_error" style="visibility: hidden; color: red;">Укажите услуги</div>',
                    '</div>',
                '</li>',
            '</ul>',
            '<div id="experience_error" style="visibility: hidden; color: red;">Укажите опыт работы по специальности</div>',
        '</div>',

        '<button class="form-control" ng-click="appendSpecialization()">Добавить специальность</button><hr>',

        '<div class="container-fluid">',
        
            // Дата рождения
            '<label>Дата рождения</label>',
            '<div class="container">',
                '<div class="row">',
                    '<div class="col-sm-6">',
                        '<div class="form-group">',
                            '<div class="input-group date" id="datetimepicker1" >',
                                '<input type="text" class="form-control" id="birth" ng-model="birth" />',
                                '<span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>',
                            '</div>',
                        '</div>',
                    '</div>',
                '</div>',
            '</div>',
            '<div id="birth_error" style="visibility: hidden; color: red;">Введите дату рождения в формате дд/мм/гггг</div></div><hr>',

        '</div>',

        // О себе
        '<div class="container-fluid">',

            '<div>',
                '<textarea class="form-control" placeholder="Расскажите о себе (не очень коротко) *" style="width: 50%; height: 10em; resize: none;" id="about" ng-model="about">',
                    '{{about}}',
                '</textarea><hr>',
                '<div id="about_error" style="visibility: hidden; color: red;">Текст слишком короткий</div>',
            '</div>',
        '</div>',

        '<a id="forward" href="#!/page2">Сохранить</a>',

        '</div>'].join(""),
    controller: ['$http', '$routeParams', '$scope', '$location',
        function page1Controller($http, $routeParams, $scope, $location) {
            $scope.first_name = "";
            $scope.last_name = "";
            $scope.middle_name = "";
            $scope.nickname = "";
            $scope.email = "";
            $scope.phone = "";
            $scope.experience = {};
            $scope.birth;
            $scope.tags = [];
            $scope.selectedSpecialization = []         

            var ctrl = this;

            // Опыт
            ctrl.experience_items = { 
                0: "", 1: "Менее года", 2: "1 год", 3: "2 года", 4: "3 года", 5: "4 года", 6: "5 лет",
                7: "6 лет", 8: "7 лет", 9: "8 лет", 10: "9 лет", 11: "10 лет", 12: "Более 10 лет" 
            };

            // Всевозможные специализации
            var specializations = [
                {
                    "spec": "3D Графика",
                    "id": 0,                    
                    "services": {
                        "Интерьеры": false,
                        "Видеодизайн": false,
                        "Экстерьеры": false,
                        "Предметная визуализация": false,
                        "3D Моделирование": false,
                        "3D Анимация": false,
                        "3D Персонажи": false,
                        "3D Иллюстрации": false
                    }
                },
                {   "spec": "Административная поддержка",
                    "id": 1,
                    "services": {
                        "Ввод и обработка данных/текста": false,
                        "Обработка писем": false,
                        "Виртуальный ассистент": false                      
                    }
                },
                {   "spec": "Анимация/Мультипликация",
                    "id": 2,
                    "services": {
                        "Баннеры": false,
                        "2D анимация": false,
                        "3D анимация": false,
                        "Раскадровки": false,
                        "Сценарии для анимации": false,
                        "Музыка/звуки": false,
                        "2D персонажи": false,
                        "3D персонажи": false,
                        "Гейм-арт": false,                    
                        
                    } 
                },
                {   "spec": "Арт",
                    "id": 3,
                    "services": {
                        "Рисунки и иллюстрации": false,
                        "2D анимация": false,
                        "Живопись": false,
                        "3D персонажи": false,
                        "Иконки": false,
                        "Комиксы": false,
                        "Аэрография": false,
                        "Граффити": false,
                        "Векторная графика": false,
                        "2D персонажи": false,                    
                        "Хенд мэйд": false,
                        "3D иллюстрации": false,
                        "Трикотажный и текстильный дизайн": false,
                        "Концепт-арт": false,
                        "Пиксел-арт": false                    
                    } 
                },
                {   "spec": "Архитектура/Интерьер",
                    "id": 4,
                    "services": {
                        "Интерьеры": false,
                        "Визуализация/ 3D": false,
                        "Макетирование": false,
                        "Архитектура": false,
                        "Ландшафтный дизайн/Генплан": false
                    }
                },
                {   "spec": "Аудио/Видео",
                    "id": 5,
                    "services": {
                        "Видеомонтаж": false,
                        "Видеодизайн": false,
                        "Диктор": false,
                        "Видеопрезентации": false,
                        "Режиссура": false,
                        "Создание субтитров": false,
                        "Кастинг": false,
                        "Музыка/Звуки": false,
                        "Видеосъемка": false,
                        "Аудиомонтаж": false,
                        "Видеоинфографика": false,
                        "Свадебное видео": false,
                        "Раскадровки": false,                                        
                    }
                },
                {   "spec": "Бизнес-услуги",
                    "id": 6,
                    "services": {
                        "Статистический анализ": false,
                        "Обработка платежей": false,
                        "Кадровый учет и зарплата": false                                   
                    }
                },
                {   "spec": "Дизайн",
                    "id": 7,
                    "services": {
                        "Дизайн сайтов": false,
                        "Логотипы": false,
                        "Фирменный стиль": false,                                  
                        "Наружная реклама": false,
                        "Интерфейсы": false,
                        "Презентации": false,                              
                        "Промышленный дизайн": false,
                        "Дизайн выставочных стендов": false,
                        "Картография": false,                                 
                        "Дизайн машинной вышивки": false,
                        "Полиграфический дизайн": false,
                        "Интерьеры": false,                                   
                        "Баннеры": false,                                   
                        "Дизайн упаковки": false,
                        "Технический дизайн": false,
                        "Ландшафтный дизайн/Генплан": false,                                
                        "Дизайн интерфейсов приложений": false,
                        "Инфографика": false,
                        "Разработка шрифтов": false
                    }
                },
                {   "spec": "Инжиниринг",
                    "id": 8,
                    "services": {
                        "Чертежи/Схемы": false,
                        "Слаботочные сети/Автоматизация": false,
                        "Электрика": false,
                        "Сметы": false,
                        "Водоснабжение/Канализация": false,
                        "Газоснабжение": false,                                  
                        "Конструкции": false,
                        "Машиностроение": false,
                        "Отопление/Вентиляция": false,                                
                        "Технология": false,
                        "Разработка радиоэлектронных систем": false
                    }
                },
                {   "spec": "Консалтинг",
                    "id": 9,
                    "services": {
                        "Юриспруденция": false,
                        "Бизнес-консультирование": false,
                        "Реклама/Маркетинг": false,
                        "Переводы/Тексты": false,
                        "Финансовое консультирование": false,
                        "Разработка сайтов": false,
                        "Программирование": false,
                        "Путешествия": false,
                        "Бухгалтерия": false,                                
                        "Репетиторы/Преподаватели": false,
                        "Психолог": false,
                        "Оптимизация (SEO)": false,                                 
                        "Юзабилити": false,                                 
                        "Системы управления предприятием (ERP-системы)": false,
                        "Дизайн/Арт": false,
                        "Стилист": false
                    },                
                },
                {   "spec": "Маркетинг и продажи",
                    "id": 10,
                    "services": {
                        "SMM(маркетинг в соцсетях)": false,
                        "Телемаркетинг и продажи по телефону": false,
                        "Исследования рынка и опросы": false,
                        "Продажи и генерация лидов": false,
                        "Креатив": false,
                        "Промо-персонал": false
                    }
                },
                {   "spec": "Менеджмент",
                    "id": 11,
                    "services": {
                        "Менеджер проектов": false,
                        "Менеджер по персоналу": false,
                        "Управление репутацией онлайн": false,
                        "Менеджер по продажам": false,
                        "Арт-директор": false
                    },
                },
                {   "spec": "Мобильные приложения",
                    "id": 12,
                    "services": {
                        "Google Android": false,
                        "Дизайн": false,
                        "Прототипирование": false,
                        "iOS": false,
                        "Windows Phone": false
                    },
                },
                {   "spec": "Обслуживание клиентов",
                    "id": 13,
                    "services": {
                        "Обслуживание клиентов и поддержка": false,
                        "Техническая поддержка": false,
                        "Обработка заказов": false,
                        "Поддержка по телефону": false
                    },
                },
                {   "spec": "Обучение",
                    "id": 14,
                    "services": {
                        "Рефераты / Курсовые / Дипломы": false,
                        "Репетиторы / Преподаватели": false
                    },
                },
                {   "spec": "Оптимизация (SEO)",
                    "id": 15,
                    "services": {
                        "Поисковые системы": false,
                        "SMO": false,
                        "SEM": false,
                        "Контекстная реклама": false,
                        "Контент": false,
                        "Продажа ссылок": false
                    },
                },
                {   "spec": "Переводы",
                    "id": 16,
                    "services": {
                        "Технический перевод": false,
                        "Художественный перевод": false,
                        "Локализация ПО, сайтов и игр": false,
                        "Редактирование переводов": false,
                        "Перевод текстов общей тематики": false,
                        "Коррекспонденция/Деловая переписка": false,
                        "Устный перевод": false
                    },
                },
                {   "spec": "Полиграфия",
                    "id": 17,
                    "services": {
                        "Полиграфический дизайн": false,
                        "Дизайн упаковки": false,
                        "Разработка шрифтов": false,
                        "Полиграфическая верстка": false,
                        "Допечатная подготовка": false,
                        "Верстка электронных изданий": false
                    },
                },
                {   "spec": "Программирование",
                    "id": 18,
                    "services": {
                        "Веб-программирование": false,
                        "C-программирование": false,
                        "Базы данных": false,
                        "Программирование для сотовых телефонов и КПК": false,
                        "Программирование игр": false,
                        "Встраиваемые системы": false,
                        "Проектирование": false,
                        "Плагины/Сценарии/Утилиты": false,
                        "Макросы для игр": false,
                        "Прикладное программирование": false,
                        "Системый администратор": false,
                        "QA(тестирование)": false,                                 
                        "Системное программирование": false,                                 
                        "Защита информации": false,
                        "Разработка CDM и ERP": false,
                        "Управление проектами разработки": false,
                        "Интерактивные приложения": false
                    },                
                },
                {   "spec": "Разработка игр",
                    "id": 19,
                    "services": {
                        "Рисунки и иллюстрации": false,
                        "3D анимация": false,
                        "Концепт/Эскизы": false,
                        "Пиксел-арт": false,
                        "Макросы для игр": false,
                        "3D моделирование": false,
                        "Flash/Flex-программирование": false,
                        "Программирование игр": false,
                        "Тестирование игр (QA)": false,
                        "Озвучивание игр": false,
                        "Видеоролики": false
                    },
                },
                {   "spec": "Разработка сайтов",
                    "id": 20,
                    "services": {
                        "Веб-программирование": false,
                        "Сайт 'под ключ'": false,
                        "Верстка": false,
                        "Менеджер проектов": false,
                        "QA (тестирование)": false,
                        "Доработка сайтов": false,
                        "Флеш-сайты": false,
                        "Адаптивный дизайн": false,
                        "Копирайтинг": false,
                        "Дизайн сайтов": false,
                        "Контент-менеджер": false,
                        "Системы администрирования (CMS)": false,                                 
                        "Интернет-магазины": false,                                 
                        "Проектирование": false,
                        "Wap/PDA-сайты": false,
                        "Юзабилити-анализ": false
                    },
                },
                {   "spec": "Реклама/Маркетинг",
                    "id": 21,
                    "services": {
                        "Контекстная реклама": false,
                        "Сбор и обрботка информации": false,
                        "PR-менеджмент": false,
                        "Бизнес-планы": false,
                        "Медиапланирование": false,
                        "SMO": false,
                        "Рекламные концепции": false,
                        "Исследования": false,
                        "Организация мероприятий": false
                    },
                },
                {   "spec": "Репетироры/преподаватели",
                    "id": 22,
                    "services": {
                        "Иностранные языки": false,
                        "Гуманитарные дисциплины": false,
                        "Технические дисциплины": false,
                        "Дошкольное образование": false
                    },
                },
                {   "spec": "Сети и информационные системы",
                    "id": 23,
                    "services": {
                        "Сетевое администрирование": false,
                        "ERP и CRM интеграции": false,
                        "Администрирование баз данных": false
                    },
                },
                {   "spec": "Тексты",
                    "id": 24,
                    "services": {
                        "Копирайтинг": false,
                        "Контент-менеджер": false,
                        "Статьи": false,
                        "Сканирование и распознавание": false,
                        "Постинг": false,
                        "Стихи/Поэмы/Эссе": false,
                        "Сценарии": false,
                        "Тексты на иностранных языках": false,
                        "Создание субтитров": false,
                        "Рерайтинг": false,
                        "Рефераты/Курсовые/Дипломы": false,
                        "Редактирование/Корректура": false,                                 
                        "Расшифровка аудио и видеозаписей": false,                                 
                        "Слоганы/Нейминг": false,
                        "Тексты/Речи/Рапорты": false,
                        "Новости/Пресс-релизы": false,
                        "ТЗ/Хелп/Мануал": false,
                        "Резюме": false                    
                    },
                },
                {   "spec": "Флеш",
                    "id": 25,
                    "services": {
                        "Баннеры": false,
                        "Flash/Flex программирование": false,
                        "Флеш-сайты": false,
                        "2D-анимация": false,
                        "Флеш-графика": false,
                        "Виртуальные туры": false                    
                    },
                },
                {   "spec": "Фотография",
                    "id": 26,
                    "services": {
                        "Ретуширование/Коллажи": false,
                        "Мероприятия/Репортажи": false,
                        "Свадебная фотосъемка": false,
                        "Модели": false,
                        "Рекламная/Постановочная": false,
                        "Художественная/арт": false,
                        "Архитектура/Интерьер": false,
                        "Промышленная фотосъемка": false,
                    },
                }
            ]

            // Компетенции
            ctrl.competentions = {};

            // Специализации текущие
            $scope.specializations = [];
            
            // Помещаем туда значения профессий из var specializations, добавляем туда индексыы
            for (var i = 0; i < specializations.length; i++) {
                $scope.specializations.push({ spec: specializations[i]["spec"], id: i });
            }

            // Услуги в специализации
            ctrl.service_item_id = [];
            // Услуги для первой специализации
            ctrl.service_items = []    

            // Массив индексаций профессий
            $scope.props = [];
            $scope.index = -1;

            //            

            $scope.about = "";

            $(function () {
                $('#datetimepicker1').datetimepicker({format: 'DD/MM/YYYY'});
            });

            // Инициализация
            $scope.init = function() {
                $scope.tags = [];
                                
                $scope.loadTags = function(query) {                     
                    //$scope.tags = query;
                };

                // Добавление специализации
                $scope.appendSpecialization = function() {
                    $scope.index++;
                    $scope.props.push($scope.index);
                    // Услуги для каждой последующей специализации
                    var s = {};
                    for (var i = 0; i < specializations.length; i++) {
                        s[i] = {}
                        $.extend(s[i], specializations[i]["services"]);s
                    }
                    ctrl.service_items.push(s);
                    
                    ctrl.service_item_id.push(0);
                };

                // This is the client-side script.

                // Initialize the HTTP request.
                var xhr = new XMLHttpRequest();
                xhr.open('get', '/read_user_data/');

                xhr.setRequestHeader("Content-type", "application/json");

                // Track the state changes of the request.
                xhr.onreadystatechange = function () {
                    var DONE = 4; // readyState 4 means the request is done.
                    var OK = 200; // status 200 is a successful return.
                    if (xhr.readyState === DONE) {
                        if (xhr.status === OK) {
                            var data = JSON.parse(xhr.responseText); // 'This is the returned text.'
                            $scope.first_name = data.first_name;
                            localStorage.setItem("comp_first_name", data.first_name);
                            $scope.last_name = data.last_name;
                            localStorage.setItem("comp_last_name", data.last_name);
                            $scope.middle_name = data.middle_name;
                            localStorage.setItem("comp_middle_name", data.middle_name);                                                     
                            $scope.username = data.username;
                            localStorage.setItem("comp_nickname", data.username);
                            $scope.email = data.email;
                            localStorage.setItem("comp_email", data.email);
                            $scope.about = data.about;
                            localStorage.setItem("comp_about", data.about);                                            
                            $scope.phone = data.phone;
                            localStorage.setItem("comp_phone", data.phone);
                            $scope.birth = data.date_of_birth;
                            $('#birth').val(data.date_of_birth);
                            localStorage.setItem("comp_birth", data.date_of_birth);

                            $scope.spec = data.spec;
                            
                            console.log($scope.spec)
                            // Для каждой специальности из сервисов с сервера
                            for (var property in $scope.spec) {
                                if ($scope.spec.hasOwnProperty(property)) {
                                    // В профессиямх ищем равную текущей, запоминаем ее индекс
                                    var ii = null;
                                    console.log($scope.spec[property]['name'], 'name')
                                    for (var t = 0; t < specializations.length; t++) {
                                        if (specializations[t]["spec"] === $scope.spec[property]['name']) {
                                            ii = t;
                                            break;
                                        }                              
                                    }

                                    // Увеличиваем общий индекс
                                    $scope.index++;
                                    // Вносим его в общий массив
                                    $scope.props.push($scope.index);

                                    // Запоминаем компетенции
                                    $scope.tags[$scope.index] = $scope.spec[property]["competences"];

                                    // Установить опыт
                                    $scope.experience[$scope.index] = $scope.spec[property]["experience"];
                                    
                                    // Запоминаем все сервисы
                                    var s = {};
                                    for (var i = 0; i < specializations.length; i++) {
                                        s[i] = {}
                                        $.extend(s[i], specializations[i]["services"]);s
                                    }
                                    
                                    // В массив сервисов вносим сервисы
                                    ctrl.service_items.push(s);
                                    // В массив вносим индекс текущей профессии
                                    console.log(ii)
                                    ctrl.service_item_id.push(ii);

                                    // Кликнуть на специальность-селект                           
                                    $scope.selectedSpecialization[$scope.index] = $scope.specializations[ii];

                                    // Установить значения чекбоксов через модель
                                    for (key in $scope.spec[property]["services"]) {
                                        value = $scope.spec[property]["services"][key];                                        
                                        console.log(ctrl.service_item_id)
                                        ctrl.service_items[$scope.index][ctrl.service_item_id[$scope.index]][value] = true;                                     
                                    }                                                                
                                }
                            }

                            $scope.$digest();
                        } else {
                            alert('Error: ' + xhr.status); // An error occurred during the request.
                        }
                    }
                };

                // Send the request to send-ajax-data.php
                xhr.send(null);                
                                
            }

            // Валидация полей при клике на кнопку Вперед
            $("a#forward").click(function(event) {
                $("#first_name_error").css({"visibility": "hidden"});
                if ($("input#first_name").val().replace(/\s/g, '').length === 0) {
                    $("#first_name_error").css({"visibility": "visible"});
                    event.preventDefault();
                }

                $("#last_name_error").css({"visibility": "hidden"});
                if ($("input#last_name").val().replace(/\s/g, '').length === 0) {
                    $("#last_name_error").css({"visibility": "visible"});
                    event.preventDefault();
                }

                $("#email_error").css({"visibility": "hidden"});
                if ($("input#email").val().replace(/\s/g, '').length === 0) {
                    $("#email_error").css({"visibility": "visible"});
                    event.preventDefault();
                }

                $("#phone_error").css({"visibility": "hidden"});
                $scope.phone = $("input#phone").val().replace(/\s/g, '');
                localStorage.setItem("comp_phone", $scope.phone);

                if ($("input#phone").val().match(/^[\+]*\d{5,15}$/) === null) {                   
                    $("#phone_error").css({"visibility": "visible"});
                    event.preventDefault();
                }

                $("#birth_error").css({"visibility": "hidden"});
                $scope.birth = $('#birth').val().replace(/\s/g, '');
                if ($('#birth').val().match(/^([\d]{2}\/){2}[\d]{4}$/) === null) {
                    $("#birth_error").css({"visibility": "visible"});
                    event.preventDefault();
                }

                $("#experience_error").css({"visibility": "hidden"});
                if (Object.keys($scope.experience).length !== $scope.index + 1) {
                    $("#experience_error").css({"visibility": "visible"});
                    event.preventDefault();
                }

                $("#about_error").css({"visibility": "hidden"});
                if ($("textarea#about").val().replace(/\s/g, '').length < 200) {
                    $("#about_error").css({"visibility": "visible"});
                    event.preventDefault();
                }

                $("#competentions_error").css({"visibility": "hidden"});

                // Сохранение значений полей в память
                localStorage.setItem("comp_first_name", $scope.first_name);
                localStorage.setItem("comp_last_name", $scope.last_name);
                localStorage.setItem("comp_middle_name", $scope.middle_name);
                localStorage.setItem("comp_nickname", $scope.nickname);
                localStorage.setItem("comp_email", $scope.email);
                localStorage.setItem("comp_phone", $scope.phone);
                localStorage.setItem("comp_about", $scope.about);
                localStorage.setItem("comp_birth", $scope.birth);
                localStorage.setItem("comp_tags", JSON.stringify($scope.tags));
                localStorage.setItem("comp_experience", JSON.stringify($scope.experience));

                var services = [], profession = [];
                for (var i = 0; i < ctrl.service_item_id.length; i++) {
                    services.push(ctrl.service_items[i][ctrl.service_item_id[i]]);
                    profession.push(specializations[ctrl.service_item_id[i]]["spec"]);
                }

                localStorage.setItem("comp_spec", JSON.stringify(services));
                localStorage.setItem("comp_spec_nums", JSON.stringify(ctrl.service_item_id));
                localStorage.setItem("comp_profession", JSON.stringify(profession));

                $scope.$digest();
            });
        }             
    ]});
