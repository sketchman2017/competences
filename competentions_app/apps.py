from __future__ import unicode_literals

from django.apps import AppConfig


class CompetentionsAppConfig(AppConfig):
    name = 'competentions_app'
