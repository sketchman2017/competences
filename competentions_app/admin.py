from django.contrib import admin

# Register your models here.

from competentions_app.models import CustomUser, Profession, Service, Competence, Question, Tag, Answer


class CustomUserAdmin(admin.ModelAdmin):
    list_display = [ 'email', 'username', 'date_of_birth', 'first_name', 'last_name', 'middle_name', 'phone', 'about' ]
    ordering = ['email']
    search_fields = ( 'email', 'username', 'date_of_birth', 'first_name', 'last_name', 'middle_name', 'phone', 'about' )

class ProfessionAdmin(admin.ModelAdmin): 
    list_display = ['name'] 
    ordering = ['name']

class ServiceAdmin(admin.ModelAdmin):
    list_display = [ 'name' ] 
    ordering = ['name']

class ExperienceAdmin(admin.ModelAdmin):
    list_display = [ 'name' ] 
    ordering = ['name']

class CompetenceAdmin(admin.ModelAdmin):
    list_display = [ 'name' ] 
    ordering = ['name']

class QuestionAdmin(admin.ModelAdmin):
    list_display = [ 'name' ] 
    ordering = ['name']

class TagAdmin(admin.ModelAdmin):
    list_display = [ 'name' ] 
    ordering = ['name']

class AnswerAdmin(admin.ModelAdmin):
    list_display = [ 'text' ] 
    ordering = ['text']

admin.site.register(CustomUser, CustomUserAdmin)
admin.site.register(Profession, ProfessionAdmin)

admin.site.register(Service, ServiceAdmin)
admin.site.register(Competence, CompetenceAdmin)
admin.site.register(Question, QuestionAdmin)
admin.site.register(Tag, TagAdmin)
admin.site.register(Answer, AnswerAdmin)
