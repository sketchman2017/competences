# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

# Define a custom User class to work with django-social-auth
from django.contrib.auth.models import AbstractBaseUser, BaseUserManager
from django.conf import settings

from datetime import datetime


class CustomUserManager(BaseUserManager):
    def create_user(self, email=None, password="", username=None, *args, **kwargs):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        if not email:
            raise ValueError('Users must have an email address')

        user = self.model(
            email=self.normalize_email(email)
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(
            email,
            password=password
        )
        user.is_admin = True
        user.save(using=self._db)
        return user


class Tag(models.Model):
    name = models.CharField(max_length=30, null=True)

    def __unicode__(self):
       return self.name

    class Meta:
        verbose_name = "tag"


class CompetenceManager(models.Manager):
    def create_competence(self, name):
        competence = self.create(name=name)
        # do something with the book
        return competence
        
class Competence(models.Model):
    name = models.CharField(max_length=30, null=True)

    objects = CompetenceManager()
    
    def __unicode__(self):
       return self.name

    class Meta:
        verbose_name = "competence"


class Question(models.Model):
    name = models.CharField(max_length=255)
    competences = models.ManyToManyField(Competence)

    def __unicode__(self):
       return self.name

    class Meta:
        verbose_name = "question"


class ServiceManager(models.Manager):
    def create_service(self, name):
        service = self.create(name=name)
        # do something with the book
        return service

class Service(models.Model):
    name = models.CharField(max_length=30, null=True)

    objects = ServiceManager()
    
    def __unicode__(self):
       return self.name

    class Meta:
        verbose_name = "service"


class CustomUser(AbstractBaseUser):
    email = models.EmailField(verbose_name='email address', max_length=255)
    username = models.CharField(max_length=255, null=True)
    date_of_birth = models.DateField(null=True)
    is_active = models.BooleanField(default=True)
    is_admin = models.BooleanField(default=False)

    first_name = models.CharField(max_length=30, null=True)
    last_name = models.CharField(max_length=30, null=True)
    middle_name = models.CharField(max_length=30, null=True)
    phone = models.CharField(max_length=20, null=True)
    about = models.TextField(null=True)

    tags = models.ManyToManyField(Tag)
    
    objects = CustomUserManager()

    USERNAME_FIELD = 'email'

    def get_full_name(self):
        # The user is identified by their email address
        return self.email

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def __str__(self):              # __unicode__ on Python 2
        return self.email

    def has_perm(self, perm, obj=None):
        "Does the user have a specific permission?"
        # Simplest possible answer: Yes, always
        return True

    def has_module_perms(self, app_label):
        "Does the user have permissions to view the app `app_label`?"
        # Simplest possible answer: Yes, always
        return True

    @property
    def is_staff(self):
        "Is the user a member of staff?"
        # Simplest possible answer: All admins are staff
        return self.is_admin

    def __unicode__(self):
       return self.email

    class Meta:
        verbose_name = "user"


class Answer(models.Model):
    text = models.TextField()
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    def __unicode__(self):
       return self.text


PROFESSION_CHOICES = (
    ('3d', u'3D Графика'),
    ('administrative', u'Административная поддержка'),
    ('animation', u'Анимация/Мультипликация'),
    ('art', u'Арт'),
    ('architecture', u"Архитектура/интерьер"),
    ('audio video', u"Аудио/Видео"),
    ('business', u'Бизнес-услуги'),
    ('design', u'Дизайн'),
    ('eneneering', u'Инжиниринг'),
    ('consulting', u'Консалтинг'),
    ('marketing', u'Маркетинг и продажи'),
    ('managment', u'Менеджмент'),
    ('mobils apps', u'Мобильные приложения'),
    ('service', u'Обслуживание клиентов'),
    ('education', u'Обучение'),
    ('seo', u'Оптимизация (SEO)'),
    ('translations', u'Переводы'),
    ('poligraphy', u'Полиграфия'),
    ('programming', u'Программирование'),
    ('gamedev', u'Разработка игр'),
    ('sitedev', u'Разработка сайтов'),
    ('advertisment', u'Реклама/Маркетинг'),
    ('tutoring', 'Репетироры/преподаватели'),
    ('networks', 'Сети и информационные системы'),
    ('texts', 'Тексты'),
    ('flash', 'Флеш'),
    ('photography', 'Фотография')
)


class Profession(models.Model):
    name = models.CharField(max_length=50, choices=PROFESSION_CHOICES)
    # Навыки по профессии
    competences = models.ManyToManyField(Competence)
    # Сервисы по профессии
    services = models.ManyToManyField(Service)
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)

    EXPERIENCE_CHOICES = (
        ("Less one", u'Менее года'),
        ("One", u'1 год'),
        ("Two", u'2 года'),
        ("Three", u'3 года'),
        ("Four", u"4 года"),
        ("Five", u"5 лет"),
        ("Six", u'6 лет'),
        ("Seven", u'7 лет'),
        ("Eight", u'8 лет'),
        ("Nine", u'9 лет'),
        ("Ten", u'10 лет'),
        ("More ten", u'Более 10 лет')
    )
      
    experience = models.CharField(max_length=30, null=True,
                                  choices=EXPERIENCE_CHOICES)

    def __unicode__(self):
       return self.name

    class Meta:
        verbose_name = "profession"
