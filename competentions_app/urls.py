from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.index, name='index'),
    url(r'^create_user/$', views.create_user, name='create_user'),
    url(r'^write_answers/$', views.write_answers, name='write_answers'),
    url(r'^read_user_data/$', views.read_user_data, name='read_user_data'),  
    url(r'^send_email/$', views.send_email, name='send_email'),
    url(r'^questions_by_competence/$', views.questions_by_competence, name='questions_by_competence'),
    url(r'^match_tag/$', views.match_tag, name='match_tag'),    
]

