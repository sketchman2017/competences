# -*- coding: utf-8 -*-
from django.shortcuts import render
from django.http import JsonResponse
from django.contrib.auth import get_user_model
from django.conf import settings
from django.core.mail import send_mail, BadHeaderError
from django.http import HttpResponse, HttpResponseRedirect
from django.template import loader
from django.core.mail import send_mass_mail
from django.contrib import messages
from django.contrib.auth.decorators import user_passes_test

import json
import datetime

from models import Competence, Question, Answer, Profession, PROFESSION_CHOICES,\
                   Service, CustomUser, Tag


def index(request):
    return render(request, 'competentions_app/index.html', {})


def create_user(request):
    if request.method == "POST":
        params = json.loads(request.body)

        # Инициализируем модель пользователя
        user = request.user

        user.first_name = params["first_name"]
        user.last_name = params["last_name"]
        user.middle_name = params["middle_name"]
        user.username = params["username"]
        user.email = params["email"]
        user.about = params["about"]
        user.phone = params["phone"]
        user.experience = params["experience"]
        
        specializations = [
            {
                "spec":  u"3D Графика",
                "id": 0,                    
                "services": {
                    u"Интерьеры": False,
                    u"Видеодизайн": False,
                    u"Экстерьеры": False,
                    u"Предметная визуализация": False,
                    u"3D Моделирование": False,
                    u"3D Анимация": False,
                    u"3D Персонажи": False,
                    u"3D Иллюстрации": False
                }
            },
            {   "spec": u"Административная поддержка",
                "id": 1,
                "services": {
                    u"Ввод и обработка данных/текста": False,
                    u"Обработка писем": False,
                    u"Виртуальный ассистент": False                      
                }
            },
            {   "spec": u"Анимация/Мультипликация",
                "id": 2,
                "services": {
                    u"Баннеры": False,
                    u"2D анимация": False,
                    u"3D анимация": False,
                    u"Раскадровки": False,
                    u"Сценарии для анимации": False,
                    u"Музыка/звуки": False,
                    u"2D персонажи": False,
                    u"3D персонажи": False,
                    u"Гейм-арт": False,                    
                    
                } 
            },
            {   "spec": u"Арт",
                "id": 3,
                "services": {
                    u"Рисунки и иллюстрации": False,
                    u"2D анимация": False,
                    u"Живопись": False,
                    u"3D персонажи": False,
                    u"Иконки": False,
                    u"Комиксы": False,
                    u"Аэрография": False,
                    u"Граффити": False,
                    u"Векторная графика": False,
                    u"2D персонажи": False,                    
                    u"Хенд мэйд": False,
                    u"3D иллюстрации": False,
                    u"Трикотажный и текстильный дизайн": False,
                    u"Концепт-арт": False,
                    u"Пиксел-арт": False                    
                } 
            },
            {   "spec": u"Архитектура/Интерьер",
                "id": 4,
                "services": {
                    u"Интерьеры": False,
                    u"Визуализация/ 3D": False,
                    u"Макетирование": False,
                    u"Архитектура": False,
                    u"Ландшафтный дизайн/Генплан": False
                }
            },
            {   "spec": u"Аудио/Видео",
                "id": 5,
                "services": {
                    u"Видеомонтаж": False,
                    u"Видеодизайн": False,
                    u"Диктор": False,
                    u"Видеопрезентации": False,
                    u"Режиссура": False,
                    u"Создание субтитров": False,
                    u"Кастинг": False,
                    u"Музыка/Звуки": False,
                    u"Видеосъемка": False,
                    u"Аудиомонтаж": False,
                    u"Видеоинфографика": False,
                    u"Свадебное видео": False,
                    u"Раскадровки": False,                                        
                }
            },
            {   "spec": u"Бизнес-услуги",
                "id": 6,
                "services": {
                    u"Статистический анализ": False,
                    u"Обработка платежей": False,
                    u"Кадровый учет и зарплата": False                                   
                }
            },
            {   "spec": u"Дизайн",
                "id": 7,
                "services": {
                    u"Дизайн сайтов": False,
                    u"Логотипы": False,
                    u"Фирменный стиль": False,                                  
                    u"Наружная реклама": False,
                    u"Интерфейсы": False,
                    u"Презентации": False,                              
                    u"Промышленный дизайн": False,
                    u"Дизайн выставочных стендов": False,
                    u"Картография": False,                                 
                    u"Дизайн машинной вышивки": False,
                    u"Полиграфический дизайн": False,
                    u"Интерьеры": False,                                   
                    u"Баннеры": False,                                   
                    u"Дизайн упаковки": False,
                    u"Технический дизайн": False,
                    u"Ландшафтный дизайн/Генплан": False,                                
                    u"Дизайн интерфейсов приложений": False,
                    u"Инфографика": False,
                    u"Разработка шрифтов": False
                }
            },
            {   "spec": u"Инжиниринг",
                "id": 8,
                "services": {
                    u"Чертежи/Схемы": False,
                    u"Слаботочные сети/Автоматизация": False,
                    u"Электрика": False,
                    u"Сметы": False,
                    u"Водоснабжение/Канализация": False,
                    u"Газоснабжение": False,                                  
                    u"Конструкции": False,
                    u"Машиностроение": False,
                    u"Отопление/Вентиляция": False,                                
                    u"Технология": False,
                    u"Разработка радиоэлектронных систем": False
                }
            },
            {   "spec": u"Консалтинг",
                "id": 9,
                "services": {
                    u"Юриспруденция": False,
                    u"Бизнес-консультирование": False,
                    u"Реклама/Маркетинг": False,
                    u"Переводы/Тексты": False,
                    u"Финансовое консультирование": False,
                    u"Разработка сайтов": False,
                    u"Программирование": False,
                    u"Путешествия": False,
                    u"Бухгалтерия": False,                                
                    u"Репетиторы/Преподаватели": False,
                    u"Психолог": False,
                    u"Оптимизация (SEO)": False,                                 
                    u"Юзабилити": False,                                 
                    u"Системы управления предприятием (ERP-системы)": False,
                    u"Дизайн/Арт": False,
                    u"Стилист": False
                },                
            },
            {   "spec": u"Маркетинг и продажи",
                "id": 10,
                "services": {
                    u"SMM(маркетинг в соцсетях)": False,
                    u"Телемаркетинг и продажи по телефону": False,
                    u"Исследования рынка и опросы": False,
                    u"Продажи и генерация лидов": False,
                    u"Креатив": False,
                    u"Промо-персонал": False
                }
            },
            {   "spec": u"Менеджмент",
                "id": 11,
                "services": {
                    u"Менеджер проектов": False,
                    u"Менеджер по персоналу": False,
                    u"Управление репутацией онлайн": False,
                    u"Менеджер по продажам": False,
                    u"Арт-директор": False
                },
            },
            {   "spec": u"Мобильные приложения",
                "id": 12,
                "services": {
                    u"Google Android": False,
                    u"Дизайн": False,
                    u"Прототипирование": False,
                    u"iOS": False,
                    u"Windows Phone": False
                },
            },
            {   "spec": u"Обслуживание клиентов",
                "id": 13,
                "services": {
                    u"Обслуживание клиентов и поддержка": False,
                    u"Техническая поддержка": False,
                    u"Обработка заказов": False,
                    u"Поддержка по телефону": False
                },
            },
            {   "spec": u"Обучение",
                "id": 14,
                "services": {
                    u"Рефераты / Курсовые / Дипломы": False,
                    u"Репетиторы / Преподаватели": False
                },
            },
            {   "spec": u"Оптимизация (SEO)",
                "id": 15,
                "services": {
                    u"Поисковые системы": False,
                    u"SMO": False,
                    u"SEM": False,
                    u"Контекстная реклама": False,
                    u"Контент": False,
                    u"Продажа ссылок": False
                },
            },
            {   "spec": u"Переводы",
                "id": 16,
                "services": {
                    u"Технический перевод": False,
                    u"Художественный перевод": False,
                    u"Локализация ПО, сайтов и игр": False,
                    u"Редактирование переводов": False,
                    u"Перевод текстов общей тематики": False,
                    u"Коррекспонденция/Деловая переписка": False,
                    u"Устный перевод": False
                },
            },
            {   "spec": u"Полиграфия",
                "id": 17,
                "services": {
                    u"Полиграфический дизайн": False,
                    u"Дизайн упаковки": False,
                    u"Разработка шрифтов": False,
                    u"Полиграфическая верстка": False,
                    u"Допечатная подготовка": False,
                    u"Верстка электронных изданий": False
                },
            },
            {   "spec": u"Программирование",
                "id": 18,
                "services": {
                    u"Веб-программирование": False,
                    u"C-программирование": False,
                    u"Базы данных": False,
                    u"Программирование для сотовых телефонов и КПК": False,
                    u"Программирование игр": False,
                    u"Встраиваемые системы": False,
                    u"Проектирование": False,
                    u"Плагины/Сценарии/Утилиты": False,
                    u"Макросы для игр": False,
                    u"Прикладное программирование": False,
                    u"Системый администратор": False,
                    u"QA(тестирование)": False,                                 
                    u"Системное программирование": False,                                 
                    u"Защита информации": False,
                    u"Разработка CDM и ERP": False,
                    u"Управление проектами разработки": False,
                    u"Интерактивные приложения": False
                },                
            },
            {   "spec": u"Разработка игр",
                "id": 19,
                "services": {
                    u"Рисунки и иллюстрации": False,
                    u"3D анимация": False,
                    u"Концепт/Эскизы": False,
                    u"Пиксел-арт": False,
                    u"Макросы для игр": False,
                    u"3D моделирование": False,
                    u"Flash/Flex-программирование": False,
                    u"Программирование игр": False,
                    u"Тестирование игр (QA)": False,
                    u"Озвучивание игр": False,
                    u"Видеоролики": False
                },
            },
            {   "spec": u"Разработка сайтов",
                "id": 20,
                "services": {
                    u"Веб-программирование": False,
                    u"Сайт 'под ключ'": False,
                    u"Верстка": False,
                    u"Менеджер проектов": False,
                    u"QA (тестирование)": False,
                    u"Доработка сайтов": False,
                    u"Флеш-сайты": False,
                    u"Адаптивный дизайн": False,
                    u"Копирайтинг": False,
                    u"Дизайн сайтов": False,
                    u"Контент-менеджер": False,
                    u"Системы администрирования (CMS)": False,                                 
                    u"Интернет-магазины": False,                                 
                    u"Проектирование": False,
                    u"Wap/PDA-сайты": False,
                    u"Юзабилити-анализ": False
                },
            },
            {   "spec": u"Реклама/Маркетинг",
                "id": 21,
                "services": {
                    u"Контекстная реклама": False,
                    u"Сбор и обрботка информации": False,
                    u"PR-менеджмент": False,
                    u"Бизнес-планы": False,
                    u"Медиапланирование": False,
                    u"SMO": False,
                    u"Рекламные концепции": False,
                    u"Исследования": False,
                    u"Организация мероприятий": False
                },
            },
            {   "spec": u"Репетироры/преподаватели",
                "id": 22,
                "services": {
                    u"Иностранные языки": False,
                    u"Гуманитарные дисциплины": False,
                    u"Технические дисциплины": False,
                    u"Дошкольное образование": False
                },
            },
            {   "spec": u"Сети и информационные системы",
                "id": 23,
                "services": {
                    u"Сетевое администрирование": False,
                    u"ERP и CRM интеграции": False,
                    u"Администрирование баз данных": False
                },
            },
            {   "spec": u"Тексты",
                "id": 24,
                "services": {
                    u"Копирайтинг": False,
                    u"Контент-менеджер": False,
                    u"Статьи": False,
                    u"Сканирование и распознавание": False,
                    u"Постинг": False,
                    u"Стихи/Поэмы/Эссе": False,
                    u"Сценарии": False,
                    u"Тексты на иностранных языках": False,
                    u"Создание субтитров": False,
                    u"Рерайтинг": False,
                    u"Рефераты/Курсовые/Дипломы": False,
                    u"Редактирование/Корректура": False,                                 
                    u"Расшифровка аудио и видеозаписей": False,                                 
                    u"Слоганы/Нейминг": False,
                    u"Тексты/Речи/Рапорты": False,
                    u"Новости/Пресс-релизы": False,
                    u"ТЗ/Хелп/Мануал": False,
                    u"Резюме": False                    
                },
            },
            {   "spec": u"Флеш",
                "id": 25,
                "services": {
                    u"Баннеры": False,
                    u"Flash/Flex программирование": False,
                    u"Флеш-сайты": False,
                    u"2D-анимация": False,
                    u"Флеш-графика": False,
                    u"Виртуальные туры": False                    
                },
            },
            {   "spec": u"Фотография",
                "id": 26,
                "services": {
                    u"Ретуширование/Коллажи": False,
                    u"Мероприятия/Репортажи": False,
                    u"Свадебная фотосъемка": False,
                    u"Модели": False,
                    u"Рекламная/Постановочная": False,
                    u"Художественная/арт": False,
                    u"Архитектура/Интерьер": False,
                    u"Промышленная фотосъемка": False,
                },
            }
        ]

        _EXPERIENCE = {
            u'Менее года': "Less one",
            u'1 год': "One",
            u'2 года': "Two",
            u'3 года': "Three",
            u"4 года": "Four",
            u"5 лет": "Five",
            u'6 лет': "Six",
            u'7 лет': "Seven",
            u'8 лет': "Eight",
            u'9 лет': "Nine",
            u'10 лет': "Ten",
            u'Более 10 лет': "More ten"
        }
        
        day = int(params["date_of_birth"][:2])
        month = int(params["date_of_birth"][3:5])
        year = int(params["date_of_birth"][6:10])
        user.date_of_birth = datetime.date(day=day, month=month, year=year)
        user.save()

        # Необходимо убрать из базы все сервисы и профессии пользователя
        p = Profession.objects.filter(user=user)
            
        for pr in p:
            pr.services.all().delete()

        for pr in p:
            pr.delete()
        
        competentions = []

        for i, v in enumerate(params["spec_nums"]):
            # Название услуг
            services = params["spec"][i]

            # Новая профессия/специальность
            prof = Profession(name=params["profession"][i], user=user)
            prof.save()

            # Для каждого сервиса
            for key, value in services.iteritems():
                # Если он отмечен галочкой
                if value == True:
                    obj = None
                    try:
                        # Достаем из базы
                        obj = Service.objects.get(name = key)
                    except Service.DoesNotExist:
                        # Или создаем новый и сохраняем
                        obj = Service(name=key)
                        obj.save()
                    # Добавляем many-to-many связь
                    prof.services.add(obj)
                else:
                    # Если он не отмечен галочкой, нужно достать из базы и
                    # убрать связь
                    obj = None
                    try:
                        obj = Service.objects.get(name = key)
                    except Service.DoesNotExist:
                        pass
                    prof.services.remove(obj)

            # Набор компетенций для специальности
            try:
                for key, value in enumerate(params["tags"][i]):
                    try:
                        # Набираем общий список компетенций
                        competentions.append(value[u'text'])
                        # Вытаскиваем из базы
                        obj = Competence.objects.get(name = value[u'text'])
                    except Competence.DoesNotExist:
                        # Или создаем новую компетенцию и сохраняем
                        obj = Competence(name=value[u'text'])
                        obj.save()
                    # Добавляем отношение many-to-many к профессии
                    prof.competences.add(obj)
            except IndexError:
                pass

            prof.experience = _EXPERIENCE[params["experience"][str(i)]]
            prof.save()
    
        questions =  Question.objects.filter(competences__name__in=competentions)

        # Получить ответы для вопросов, если они есть
        answers = []
        for k, q in enumerate(questions):        
            try:
                obj = Answer.objects.get(question=q, user=request.user)
                answers.append(obj.text)
            except Answer.DoesNotExist:
                answers.append("")

        questions = [value.name for key, value in enumerate(questions)]
        questions = list(set(questions))
        questions = json.dumps(questions)

        answers = json.dumps(answers)
        return JsonResponse({"questions": questions, "answers": answers})
        
    return JsonResponse({})


def write_answers(request):
    if request.method == "POST":
        params = json.loads(request.body)
        for question, answer in params.iteritems():
            q = Question.objects.get(name=question)
            # Находим существующий или создаем новый объект ответа, изменяем, сохраняем
            try:
                obj = Answer.objects.get(question=q, user=request.user)
                obj.text=answer
                obj.save()
            except Answer.DoesNotExist:                
                obj = Answer(text=answer, question=q, user=request.user)
                obj.save()
        user = get_user_model()
        return JsonResponse({ "saved": "ok" })
    return JsonResponse({})
    
EXPERIENCE = {
    "Less one": u'Менее года',
    "One": u'1 год',
    "Two": u'2 года',
    "Three": u'3 года',
    "Four": u"4 года",
    "Five": u"5 лет",
    "Six": u'6 лет',
    "Seven": u'7 лет',
    "Eight": u'8 лет',
    "Nine": u'9 лет',
    "Ten": u'10 лет',
    "More ten": u'Более 10 лет'
}


user_passes_test(lambda u: u.is_superuser)
def read_user_data(request):
    if request.method == "GET":
        user = request.user
        birth = str(user.date_of_birth)
                
        # Профессии пользователя
        profs = Profession.objects.filter(user=user)
        profession = []        

        for i, p in enumerate(profs):
            # Компетенции профессии
            competences = p.competences.all()
            competences_list = []
            for i, value in enumerate(competences):
                competences_list.append(value.name)

            pr = {}
            pr["competences"] = competences_list
            print p.experience            
            pr["experience"] = EXPERIENCE[p.experience]

            # Услуги профессии
            services_list = []
            services = p.services.all()
            for i, service in enumerate(services):
                services_list.append(service.name)

            pr["services"] = services_list
            pr["name"] = p.name
            profession.append(pr)

        if birth == "None":
            birth = ""
        else:
            birth = "/".join([birth[8:10], birth[5:7], birth[:4]])

        return JsonResponse({
            "first_name": user.first_name,
            "last_name": user.last_name,
            "middle_name": user.middle_name,
            "username": user.username,
            "email": user.email,
            "about": user.about,
            "phone": user.phone,
            "date_of_birth": birth,
            "spec": profession
       })


user_passes_test(lambda u: u.is_superuser)
def send_email(request):
    if request.method == "POST":
        subject = request.POST.get('subject', '')
        message = request.POST.get('message', '')
        from_email = request.POST.get('from_email', '')
        try:        
            recipient_list = dict(request.POST)[u'recipient_list[]']
        except KeyError:
            return HttpResponse('Введите корректно поля формы.')            
        
        if subject and message and from_email and len(recipient_list) > 0:
            try:
                send_mail(subject, message, from_email, recipient_list, fail_silently=False)
                messages.success(request, 'Письма отправлены.')
            except BadHeaderError:
                return HttpResponse('Invalid header found.')
            return HttpResponseRedirect('/competences/')
        else:
            # In reality we'd use a form class
            # to get proper validation errors.
            return HttpResponse('Введите корректно поля формы.')
            
    if request.method == "GET":
        t = loader.get_template('competentions_app/email.html')
        c = {'users': CustomUser.objects.all()}
        return HttpResponse(t.render(c, request), content_type='text/html')
        

def questions_by_competence(request):
    if request.method == "POST":
        params = json.loads(request.body)
        competence = params['competence']
        if competence:
            questions = Question.objects.filter(competences__name__in=[competence])
            questions = [value.name for key, value in enumerate(questions)]
            questions = json.dumps(questions)
            return JsonResponse({"questions": questions})
        else:
            # In reality we'd use a form class
            # to get proper validation errors.
            return JsonResponse({"result": 'Make sure all fields are entered and valid.'})
            
    if request.method == "GET":
        t = loader.get_template('competentions_app/questions_by_competencion.html')
        c = {'competences': Competence.objects.all()}
        return HttpResponse(t.render(c, request), content_type='text/html')
        

user_passes_test(lambda u: u.is_superuser)
def match_tag(request):
    if request.method == "POST":
        params = json.loads(request.body)
        email = params['user']
        tag = params['tag']        
        if email and tag:
            u = CustomUser.objects.get(email=email)
            t = Tag.objects.create(name=tag)
            u.tags.add(t)
            u.save()
            return JsonResponse({"result": "ok"})
        else:
            # In reality we'd use a form class
            # to get proper validation errors.
            return JsonResponse({"result": 'Make sure all fields are entered and valid.'})
            
    if request.method == "GET":
        t = loader.get_template('competentions_app/tags.html')
        c = {'users': CustomUser.objects.all()}
        return HttpResponse(t.render(c, request), content_type='text/html')
