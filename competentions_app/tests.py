# -*- coding: UTF-8 -*-

from django.test import TestCase

# Create your tests here.

import unittest
import json

from django.test import Client

from models import CustomUser, Competence, Question


class SimpleTest(unittest.TestCase):
    def setUp(self):
        # Every test needs a client.
        self.client = Client()

    def test_answers(self):
        # Создание пользователя
        user, created = CustomUser.objects.get_or_create( email='sketchman2017@gmail.com' ) 
        user.set_password( '123456ab' )
        user.is_admin = True
        user.save()
        
        # Создание компетенций и вопросов
        c2 = Competence.objects.create(name="234")
        c1 = Competence.objects.create(name="123")
        c3 = Competence.objects.create(name="345")                
        c4 = Competence.objects.create(name="tag")
        c5 = Competence.objects.create(name="teag")                
        
        q1 = Question.objects.create(name="How to draw in canvas?")
        q2 = Question.objects.create(name="What is a purpuse of identations?")
        q3 = Question.objects.create(name="What is prototype?")
        
        q1.competences.add(c1)
        q1.save()
        
        q2.competences.add(c2)
        q2.save()

        q3.competences.add(c3)
        q3.save()

        # Issue a GET request.
        response = self.client.get('/accounts/login/')
        self.assertEqual(response.status_code, 200)
                
        response = self.client.post('/accounts/login/',
            {'login': 'sketchman2017@gmail.com', 'password': '123456ab'})
        self.assertEqual(response.status_code, 302)

        user = CustomUser.objects.get(email='sketchman2017@gmail.com')
        assert user.is_authenticated()

        response = self.client.get('/competences/')
        self.assertEqual(response.status_code, 200)
        
        # Чтение и проверка пустых данных пользователя
        response = self.client.get('/read_user_data/')
        self.assertEqual(response.status_code, 200)
              
        response = json.loads(response.content)
        self.assertEqual(response['username'], None)
        self.assertEqual(response['first_name'], None)
        self.assertEqual(response['last_name'], None)
        self.assertEqual(response['middle_name'], None)
        self.assertEqual(response['about'], None)
        self.assertEqual(response['spec'], [])
        self.assertEqual(response['phone'], None)
        self.assertEqual(response['date_of_birth'], '')
        self.assertEqual(response['email'], 'sketchman2017@gmail.com')
        
        # Формирование post запроса с данными пользователя
        post_data = {
            "first_name":"1111",
            "last_name":"1111",
            "middle_name":"middle",
            "username":"",
            "email":"sketchman2017@gmail.com",
             "about":u"вввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввв",
            "phone":"7878788787",
            "date_of_birth":"17/10/2016",
            "experience":{"0":"Менее года","1":"2 года","2":"1 год"},
            "tags":[[{"text":"teag"}],[{"text":"123"}],[{"text":"345"},{"text":"tag"}]],
            "spec":[{"Интерьеры":False,"Видеодизайн":True,"Экстерьеры":False,"Предметная визуализация":False,"3D Моделирование":False,"3D Анимация":False,"3D Персонажи":False,"3D Иллюстрации":False},
                    {"Юриспруденция":True,"Бизнес-консультирование":True,"Реклама/Маркетинг":True,"Переводы/Тексты":False,"Финансовое консультирование":False,"Разработка сайтов":False,"Программирование":False,"Путешествия":False,"Бухгалтерия":False,"Репетиторы/Преподаватели":False,"Психолог":False,"Оптимизация (SEO)":False,"Юзабилити":False,"Системы управления предприятием (ERP-системы)":False,"Дизайн/Арт":False,"Стилист":False},
                    {"Рефераты / Курсовые / Дипломы":True,"Репетиторы / Преподаватели":True}],
            "profession":["3D Графика","Консалтинг","Обучение"],
            "spec_nums":[0,9,14]
        }
        
        response = self.client.post('/create_user/',
            json.dumps(post_data), 'application/json')
        
        self.assertEqual(response.status_code, 200)
        
        # Проверка ответа на наличие вопросов
        response = json.loads(response.content)  
        self.assertEqual(json.loads(response["questions"]), ['What is prototype?', 'How to draw in canvas?'])
        self.assertEqual(json.loads(response["answers"]), ['', ''])
        
        # Запись ответов на вопросы в базу
        post_data= {
            'What is prototype?': "Answer 1", 'How to draw in canvas?': "Answer 2"
        }
            
        response = self.client.post('/write_answers/',
            json.dumps(post_data), 'application/json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content), {"saved": "ok"})
        
        Competence.objects.all().delete()
        Question.objects.all().delete()
        CustomUser.objects.all().delete()

    def test_answers2(self):
        # Создание пользователя
        user, created = CustomUser.objects.get_or_create( email='sketchman2017@gmail.com' ) 
        user.set_password( '123456ab' )
        user.is_admin = True
        user.save()
        
        # Создание компетенций и вопросов
        c2 = Competence.objects.create(name="234")
        c4 = Competence.objects.create(name="tag")
        c5 = Competence.objects.create(name="teag")                
        
        q2 = Question.objects.create(name="What is a purpuse of identations?")
        
        q2.competences.add(c2)
        q2.save()

        # Issue a GET request.
        response = self.client.get('/accounts/login/')
        self.assertEqual(response.status_code, 200)
                
        response = self.client.post('/accounts/login/',
            {'login': 'sketchman2017@gmail.com', 'password': '123456ab'})
        self.assertEqual(response.status_code, 302)

        user = CustomUser.objects.get(email='sketchman2017@gmail.com')
        assert user.is_authenticated()

        response = self.client.get('/competences/')
        self.assertEqual(response.status_code, 200)
        
        # Чтение и проверка пустых данных пользователя
        response = self.client.get('/read_user_data/')
        self.assertEqual(response.status_code, 200)
              
        response = json.loads(response.content)
        self.assertEqual(response['username'], None)
        self.assertEqual(response['first_name'], None)
        self.assertEqual(response['last_name'], None)
        self.assertEqual(response['middle_name'], None)
        self.assertEqual(response['about'], None)
        self.assertEqual(response['spec'], [])
        self.assertEqual(response['phone'], None)
        self.assertEqual(response['date_of_birth'], '')
        self.assertEqual(response['email'], 'sketchman2017@gmail.com')
        
        # Формирование post запроса с данными пользователя
        post_data = {
            "first_name":"1111",
            "last_name":"1111",
            "middle_name":"middle",
            "username":"",
            "email":"sketchman2017@gmail.com",
             "about":u"вввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввв",
            "phone":"7878788787",
            "date_of_birth":"17/10/2016",
            "experience":{"0":"Менее года","1":"2 года","2":"1 год"},
            "tags":[[{"text":"teag"}],[{"text":"123"}],[{"text":"345"},{"text":"tag"}]],
            "spec":[{"Интерьеры":False,"Видеодизайн":True,"Экстерьеры":False,"Предметная визуализация":False,"3D Моделирование":False,"3D Анимация":False,"3D Персонажи":False,"3D Иллюстрации":False},
                    {"Юриспруденция":True,"Бизнес-консультирование":True,"Реклама/Маркетинг":True,"Переводы/Тексты":False,"Финансовое консультирование":False,"Разработка сайтов":False,"Программирование":False,"Путешествия":False,"Бухгалтерия":False,"Репетиторы/Преподаватели":False,"Психолог":False,"Оптимизация (SEO)":False,"Юзабилити":False,"Системы управления предприятием (ERP-системы)":False,"Дизайн/Арт":False,"Стилист":False},
                    {"Рефераты / Курсовые / Дипломы":True,"Репетиторы / Преподаватели":True}],
            "profession":["3D Графика","Консалтинг","Обучение"],
            "spec_nums":[0,9,14]
        }
        
        response = self.client.post('/create_user/',
            json.dumps(post_data), 'application/json')
        
        self.assertEqual(response.status_code, 200)
        
        # Проверка ответа на наличие вопросов
        response = json.loads(response.content)  
        self.assertEqual(json.loads(response["questions"]), [])
        self.assertEqual(json.loads(response["answers"]), [])
        
        Competence.objects.all().delete()
        Question.objects.all().delete()
        CustomUser.objects.all().delete()

    def test_answers3(self):
        # Создание пользователя
        user, created = CustomUser.objects.get_or_create( email='sketchman2017@gmail.com' ) 
        user.set_password( '123456ab' )
        user.is_admin = True
        user.save()
        
        # Создание компетенций и вопросов
        c1 = Competence.objects.create(name="1234")
        c2 = Competence.objects.create(name="234")
        c3 = Competence.objects.create(name="345")                
        c4 = Competence.objects.create(name="tag")
        c5 = Competence.objects.create(name="teag")                
        
        q1 = Question.objects.create(name="How to draw in canvas?")
        q2 = Question.objects.create(name="What is a purpuse of identations?")
        q3 = Question.objects.create(name="What is prototype?")
        
        q1.competences.add(c1)
        q1.save()
        
        q2.competences.add(c2)
        q2.save()

        q3.competences.add(c3)
        q3.save()

        # Issue a GET request.
        response = self.client.get('/accounts/login/')
        self.assertEqual(response.status_code, 200)
                
        response = self.client.post('/accounts/login/',
            {'login': 'sketchman2017@gmail.com', 'password': '123456ab'})
        self.assertEqual(response.status_code, 302)

        user = CustomUser.objects.get(email='sketchman2017@gmail.com')
        assert user.is_authenticated()

        response = self.client.get('/competences/')
        self.assertEqual(response.status_code, 200)
        
        # Чтение и проверка пустых данных пользователя
        response = self.client.get('/read_user_data/')
        self.assertEqual(response.status_code, 200)
              
        response = json.loads(response.content)
        self.assertEqual(response['username'], None)
        self.assertEqual(response['first_name'], None)
        self.assertEqual(response['last_name'], None)
        self.assertEqual(response['middle_name'], None)
        self.assertEqual(response['about'], None)
        self.assertEqual(response['spec'], [])
        self.assertEqual(response['phone'], None)
        self.assertEqual(response['date_of_birth'], '')
        self.assertEqual(response['email'], 'sketchman2017@gmail.com')
        
        # Формирование post запроса с данными пользователя
        post_data = {
            "first_name":"1111",
            "last_name":"1111",
            "middle_name":"middle",
            "username":"",
            "email":"sketchman2017@gmail.com",
             "about":u"вввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввв",
            "phone":"7878788787",
            "date_of_birth":"17/10/2016",
            "experience":{"0":"Менее года","1":"2 года","2":"1 год"},
            "tags":[[{"text":"teag"}],[{"text":"123"}],[{"text":"345"},{"text":"tag"}]],
            "spec":[{"Интерьеры":False,"Видеодизайн":True,"Экстерьеры":False,"Предметная визуализация":False,"3D Моделирование":False,"3D Анимация":False,"3D Персонажи":False,"3D Иллюстрации":False},
                    {"Юриспруденция":True,"Бизнес-консультирование":True,"Реклама/Маркетинг":True,"Переводы/Тексты":False,"Финансовое консультирование":False,"Разработка сайтов":False,"Программирование":False,"Путешествия":False,"Бухгалтерия":False,"Репетиторы/Преподаватели":False,"Психолог":False,"Оптимизация (SEO)":False,"Юзабилити":False,"Системы управления предприятием (ERP-системы)":False,"Дизайн/Арт":False,"Стилист":False},
                    {"Рефераты / Курсовые / Дипломы":True,"Репетиторы / Преподаватели":True}],
            "profession":["3D Графика","Консалтинг","Обучение"],
            "spec_nums":[0,9,14]
        }
        
        response = self.client.post('/create_user/',
            json.dumps(post_data), 'application/json')
        
        self.assertEqual(response.status_code, 200)
        
        # Проверка ответа на наличие вопросов
        response = json.loads(response.content)  
        self.assertEqual(json.loads(response["questions"]), ['What is prototype?'])
        self.assertEqual(json.loads(response["answers"]), [''])
        
        # Запись ответов на вопросы в базу
        post_data= {
            'What is prototype?': "Answer 1", 'How to draw in canvas?': "Answer 2"
        }
            
        response = self.client.post('/write_answers/',
            json.dumps(post_data), 'application/json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content), {"saved": "ok"})
        
        Competence.objects.all().delete()
        Question.objects.all().delete()
        CustomUser.objects.all().delete()

    def test_answers4(self):
        # Создание пользователя
        user, created = CustomUser.objects.get_or_create( email='sketchman2017@gmail.com' ) 
        user.set_password( '123456ab' )
        user.is_admin = True
        user.save()
        
        # Создание компетенций и вопросов
        c2 = Competence.objects.create(name="234")
        c1 = Competence.objects.create(name="123")
        c3 = Competence.objects.create(name="345")                
        c4 = Competence.objects.create(name="tag")
        c5 = Competence.objects.create(name="teag")                
        
        q1 = Question.objects.create(name="How to draw in canvas?")
        q2 = Question.objects.create(name="What is a purpuse of identations?")
        q3 = Question.objects.create(name="What is prototype?")
        
        q1.competences.add(c1)
        q1.save()
        
        q2.competences.add(c3)
        q2.save()

        q3.competences.add(c5)
        q3.save()

        # Issue a GET request.
        response = self.client.get('/accounts/login/')
        self.assertEqual(response.status_code, 200)
                
        response = self.client.post('/accounts/login/',
            {'login': 'sketchman2017@gmail.com', 'password': '123456ab'})
        self.assertEqual(response.status_code, 302)

        user = CustomUser.objects.get(email='sketchman2017@gmail.com')
        assert user.is_authenticated()

        response = self.client.get('/competences/')
        self.assertEqual(response.status_code, 200)
        
        # Чтение и проверка пустых данных пользователя
        response = self.client.get('/read_user_data/')
        self.assertEqual(response.status_code, 200)
              
        response = json.loads(response.content)
        self.assertEqual(response['username'], None)
        self.assertEqual(response['first_name'], None)
        self.assertEqual(response['last_name'], None)
        self.assertEqual(response['middle_name'], None)
        self.assertEqual(response['about'], None)
        self.assertEqual(response['spec'], [])
        self.assertEqual(response['phone'], None)
        self.assertEqual(response['date_of_birth'], '')
        self.assertEqual(response['email'], 'sketchman2017@gmail.com')
        
        # Формирование post запроса с данными пользователя
        post_data = {
            "first_name":"1111",
            "last_name":"1111",
            "middle_name":"middle",
            "username":"",
            "email":"sketchman2017@gmail.com",
             "about":u"вввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввв",
            "phone":"7878788787",
            "date_of_birth":"17/10/2016",
            "experience":{"0":"Менее года","1":"2 года","2":"1 год"},
            "tags":[[{"text":"teag"}],[{"text":"123"}],[{"text":"345"},{"text":"tag"}]],
            "spec":[{"Интерьеры":False,"Видеодизайн":True,"Экстерьеры":False,"Предметная визуализация":False,"3D Моделирование":False,"3D Анимация":False,"3D Персонажи":False,"3D Иллюстрации":False},
                    {"Юриспруденция":True,"Бизнес-консультирование":True,"Реклама/Маркетинг":True,"Переводы/Тексты":False,"Финансовое консультирование":False,"Разработка сайтов":False,"Программирование":False,"Путешествия":False,"Бухгалтерия":False,"Репетиторы/Преподаватели":False,"Психолог":False,"Оптимизация (SEO)":False,"Юзабилити":False,"Системы управления предприятием (ERP-системы)":False,"Дизайн/Арт":False,"Стилист":False},
                    {"Рефераты / Курсовые / Дипломы":True,"Репетиторы / Преподаватели":True}],
            "profession":["3D Графика","Консалтинг","Обучение"],
            "spec_nums":[0,9,14]
        }
        
        response = self.client.post('/create_user/',
            json.dumps(post_data), 'application/json')
        
        self.assertEqual(response.status_code, 200)
        
        # Проверка ответа на наличие вопросов
        response = json.loads(response.content)  
        self.assertEqual(json.loads(response["questions"]), [u'What is prototype?', u'How to draw in canvas?', u'What is a purpuse of identations?'])
        self.assertEqual(json.loads(response["answers"]), ['', '', ''])
        
        # Запись ответов на вопросы в базу
        post_data= {
            'What is prototype?': "Answer 1", 'How to draw in canvas?': "Answer 2"
        }
            
        response = self.client.post('/write_answers/',
            json.dumps(post_data), 'application/json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content), {"saved": "ok"})
        
        Competence.objects.all().delete()
        Question.objects.all().delete()
        CustomUser.objects.all().delete()
        
    def test_answers5(self):
        # Создание пользователя
        user, created = CustomUser.objects.get_or_create( email='sketchman2017@gmail.com' ) 
        user.set_password( '123456ab' )
        user.is_admin = True
        user.save()
        
        # Создание компетенций и вопросов
        c2 = Competence.objects.create(name="234")
        c1 = Competence.objects.create(name="123")
        c3 = Competence.objects.create(name="345")                
        c4 = Competence.objects.create(name="tag")
        c5 = Competence.objects.create(name="teag")                
        
        q1 = Question.objects.create(name="How to draw in canvas?")
        q2 = Question.objects.create(name="What is a purpuse of identations?")
        q3 = Question.objects.create(name="What is prototype?")
        

        q2.competences.add(c4)
        q2.save()

        q3.competences.add(c5)
        q3.save()

        # Issue a GET request.
        response = self.client.get('/accounts/login/')
        self.assertEqual(response.status_code, 200)
                
        response = self.client.post('/accounts/login/',
            {'login': 'sketchman2017@gmail.com', 'password': '123456ab'})
        self.assertEqual(response.status_code, 302)

        user = CustomUser.objects.get(email='sketchman2017@gmail.com')
        assert user.is_authenticated()

        response = self.client.get('/competences/')
        self.assertEqual(response.status_code, 200)
        
        # Чтение и проверка пустых данных пользователя
        response = self.client.get('/read_user_data/')
        self.assertEqual(response.status_code, 200)
              
        response = json.loads(response.content)
        self.assertEqual(response['username'], None)
        self.assertEqual(response['first_name'], None)
        self.assertEqual(response['last_name'], None)
        self.assertEqual(response['middle_name'], None)
        self.assertEqual(response['about'], None)
        self.assertEqual(response['spec'], [])
        self.assertEqual(response['phone'], None)
        self.assertEqual(response['date_of_birth'], '')
        self.assertEqual(response['email'], 'sketchman2017@gmail.com')
        
        # Формирование post запроса с данными пользователя
        post_data = {
            "first_name":"1111",
            "last_name":"1111",
            "middle_name":"middle",
            "username":"",
            "email":"sketchman2017@gmail.com",
             "about":u"вввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввввв",
            "phone":"7878788787",
            "date_of_birth":"17/10/2016",
            "experience":{"0":"Менее года","1":"2 года","2":"1 год"},
            "tags":[[{"text":"teag"}],[{"text":"123"}],[{"text":"345"},{"text":"tag"}]],
            "spec":[{"Интерьеры":False,"Видеодизайн":True,"Экстерьеры":False,"Предметная визуализация":False,"3D Моделирование":False,"3D Анимация":False,"3D Персонажи":False,"3D Иллюстрации":False},
                    {"Юриспруденция":True,"Бизнес-консультирование":True,"Реклама/Маркетинг":True,"Переводы/Тексты":False,"Финансовое консультирование":False,"Разработка сайтов":False,"Программирование":False,"Путешествия":False,"Бухгалтерия":False,"Репетиторы/Преподаватели":False,"Психолог":False,"Оптимизация (SEO)":False,"Юзабилити":False,"Системы управления предприятием (ERP-системы)":False,"Дизайн/Арт":False,"Стилист":False},
                    {"Рефераты / Курсовые / Дипломы":True,"Репетиторы / Преподаватели":True}],
            "profession":["3D Графика","Консалтинг","Обучение"],
            "spec_nums":[0,9,14]
        }
        
        response = self.client.post('/create_user/',
            json.dumps(post_data), 'application/json')
        
        self.assertEqual(response.status_code, 200)
        
        # Проверка ответа на наличие вопросов
        response = json.loads(response.content)  
        self.assertEqual(json.loads(response["questions"]), [u'What is prototype?', u'What is a purpuse of identations?'])
        self.assertEqual(json.loads(response["answers"]), ['', ''])
        
        # Запись ответов на вопросы в базу
        post_data= {
            'What is prototype?': "Answer 1", 'How to draw in canvas?': "Answer 2"
        }
            
        response = self.client.post('/write_answers/',
            json.dumps(post_data), 'application/json')
        self.assertEqual(response.status_code, 200)
        self.assertEqual(json.loads(response.content), {"saved": "ok"})
        
        Competence.objects.all().delete()
        Question.objects.all().delete()
        CustomUser.objects.all().delete()

